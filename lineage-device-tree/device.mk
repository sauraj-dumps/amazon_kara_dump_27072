#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := tv

# Rootdir
PRODUCT_PACKAGES += \
    devcfg.sh \
    read_lifetime.sh \
    wifi_bridged.sh \
    wifi_common.sh \
    wifi_hostap.sh \
    wifi_log_levels.sh \

PRODUCT_PACKAGES += \
    fstab.mt8696 \
    factory_init.project.rc \
    factory_init.rc \
    init.connectivity.rc \
    init.modem.rc \
    init.mt8696.rc \
    init.mt8696.usb.rc \
    init.nvdata.rc \
    init.project.rc \
    meta_init.modem.rc \
    meta_init.project.rc \
    meta_init.rc \
    multi_init.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/amazon/kara/kara-vendor.mk)
