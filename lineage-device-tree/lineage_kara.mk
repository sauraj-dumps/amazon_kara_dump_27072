#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from kara device
$(call inherit-product, device/amazon/kara/device.mk)

PRODUCT_DEVICE := kara
PRODUCT_NAME := lineage_kara
PRODUCT_BRAND := Amazon
PRODUCT_MODEL := AFTKA
PRODUCT_MANUFACTURER := amazon

PRODUCT_GMS_CLIENTID_BASE := android-amazon

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="kara-user 7.0 PS7273 2625 amz-p,release-keys"

BUILD_FINGERPRINT := Amazon/kara/kara:7.0/PS7273/2625N:user/amz-p,release-keys
