#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_kara.mk

COMMON_LUNCH_CHOICES := \
    lineage_kara-user \
    lineage_kara-userdebug \
    lineage_kara-eng
